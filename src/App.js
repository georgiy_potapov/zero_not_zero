import React, { Component } from 'react';
import Cell from './Cell'
import Constants from './Constants'

const beginState = {
  cells : [],
  ticCounter : 0,
  tacCounter : 0,
  currentMove : Constants.currentMove.tic,
  busyCounter : 0,
  win : false
}

for(let i = 0; i < 9; i++)
  beginState.cells.push(Constants.cellState.EMPTY)

class App extends Component {

  constructor(){
    super();

    this.state = beginState;

  }

  render() {
    let tableClass = ( this.state.busyCounter === 9 || this.state.win ) 
                      ? "field-table noClick" : "field-table";
    let footerClass = ( this.state.win ) ? "footer" : "footer invisible";
    return (<React.Fragment>
              <div className="head">  
                  <div className="button">
                    <button onClick={this.resetField}>сброс</button>
                  </div>
                <div className="info">
                  <p>Количество крестиков на поле - {this.state.ticCounter}</p>
                  <p>Количество ноликов на поле - {this.state.tacCounter}</p>
                </div>
              </div>
              <div className={tableClass}>
                <div className="field-row">
                  <Cell pid={0} label={this.state.cells[0]} getMove={this.getMove} /> 
                  <Cell pid={1} label={this.state.cells[1]} getMove={this.getMove} /> 
                  <Cell pid={2} label={this.state.cells[2]} getMove={this.getMove} /> 
                </div>
                <div className="field-row">
                  <Cell pid={3} label={this.state.cells[3]} getMove={this.getMove} /> 
                  <Cell pid={4} label={this.state.cells[4]} getMove={this.getMove} /> 
                  <Cell pid={5} label={this.state.cells[5]} getMove={this.getMove} /> 
                </div>
                <div className="field-row">
                  <Cell pid={6} label={this.state.cells[6]} getMove={this.getMove} /> 
                  <Cell pid={7} label={this.state.cells[7]} getMove={this.getMove} /> 
                  <Cell pid={8} label={this.state.cells[8]} getMove={this.getMove} /> 
                </div>  
              </div>
              <div className={footerClass}>
                <p>Победа!</p>
              </div>

            </React.Fragment>
            );
  }

  getMove = (ind) => {

    let newCells = this.state.cells.map( (currentValue, index, array) => {
      if(index === ind)
        return (this.state.currentMove === Constants.currentMove.tic) ? Constants.cellState.TIC : Constants.cellState.TAC;

        return currentValue;
    });

    let newTicCounter = this.state.ticCounter;
    let newTacCounter = this.state.tacCounter;

    if( this.state.currentMove === Constants.currentMove.tic )
    newTicCounter++;
    else
    newTacCounter++;

    let newBusyCounter = this.state.busyCounter + 1;

    let newCurrentMove = (this.state.currentMove === Constants.currentMove.tic )
                      ? Constants.currentMove.tac : Constants.currentMove.tic;

    let win = this.calcWinner(newCells);      

    this.setState({
        cells : newCells,
        ticCounter : newTicCounter,
        tacCounter : newTacCounter,
        currentMove : newCurrentMove,
        busyCounter : newBusyCounter,
        win : win
    });
  }

  resetField = () => {
    this.setState(beginState);
  }

  calcWinner(cells) {

    const lines = [
     [0, 1, 2],
     [3, 4, 5],
     [6, 7, 8],
     [0, 3, 6],
     [1, 4, 7],
     [2, 5, 8],
     [0, 4, 8],
     [2, 4, 6]
    ];
    
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if( cells[a] !== Constants.cellState.EMPTY &&
          cells[a] === cells[b] &&
          cells[a] === cells[c] ) 
      {
        return true;
      }
    }
    
    return false;
  }
}

export default App;
