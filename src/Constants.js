export default class Constants {

    static cellState = {
        TIC : "X", // Крестик
        TAC : "O", // Нолик
        EMPTY : ""
    }

    static currentMove = {
        tac : "tac", //нолик
        tic : "tic" //крестик
    }
}