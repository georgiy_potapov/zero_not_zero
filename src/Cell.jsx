import React, { Component } from 'react';
import Constants from './Constants';

export default class Cell extends Component {

  render() {
    return (<div className="field-cell" onClick={this.cellClick}>{this.props.label}</div>);
  }

  cellClick = (event) => {
    if( this.props.label !== Constants.cellState.EMPTY )
        return;

    this.props.getMove(this.props.pid);
  }

}